#!/bin/bash
#   list files in the S3 bucket.

cat <<EOF
You should see the following file.

./lists3.sh
  app.env.enc
  appadmin-id_rsa.pub
  extlogin-ca.crt
  extlogin.crt
  extlogin.key.enc
  extlogin.tar.gz.enc
  other.env
EOF

#   
. ./vars.env
echo " "
echo "Listing S3 files in $S3BUCKET"
echo " "

aws s3 ls $S3BUCKET
