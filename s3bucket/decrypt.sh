#!/bin/bash
. ./enckey.env
openssl aes-256-cfb1 -pbkdf2 -d -a -k $ENCKEY -in extlogin.tar.gz.enc -out extlogin.tar.gz
