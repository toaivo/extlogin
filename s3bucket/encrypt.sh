#!/bin/bash
#  encrypt files before copy to s3 bucket
#  Must use the /usr/local/opt/openssl/bin/openssl on MAC
#  extlogin.tar (app folder)  
#  app.env - this is oracle IDCS credential used by the application
#  extlogin.key

if [ ! -f enckey.env ] ; then
   echo "File enckey.env doesn't exist."
   exit 1
fi

if [ ! -f ../vars.env ] ; then
   echo "File ../vars.env doesn't exist."
   exit 1
fi
. ./enckey.env
. ../vars.env

checkflag=0

if [ ! -f $APPENVFILE ] ; then
   echo "File $APPENVFILE doesn't exist."
   checkflag=1
fi

if [ ! -f $SSLKEY ] ; then
   echo "File $SSLKEY doesn't exist."
   checkflag=1
fi

if [ ! -f $APPTARFILE ] ; then
   echo "File $APPTARFILE doesn't exist."
   checkflag=1
fi

if [ $checkflag -eq 1 ] ; then
   exit 1
fi

export PATH=/usr/local/opt/openssl/bin:$PATH
echo "Encrypting $APPTARFILE"
openssl aes-256-cfb1 -pbkdf2 -a -salt -k $ENCKEY -in $APPTARFILE -out ${APPTARFILE}.enc
echo "Encrypting $APPENVFILE"
openssl aes-256-cfb1 -pbkdf2 -a -salt -k $ENCKEY -in $APPENVFILE -out ${APPENVFILE}.enc
echo "Encrypting $SSLKEY"
openssl aes-256-cfb1 -pbkdf2 -a -salt -k $ENCKEY -in $SSLKEY -out ${SSLKEY}.enc

