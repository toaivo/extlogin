#!/bin/sh
#  copy files to S3 BUCKET
#  This step should run after running encrypt.sh script

. ../vars.env
 
checkflag=0
if [ ! -f $APPSSH ] ; then
   echo "File $APPSSH does not exist."
   checkflag=1
fi

if [ ! -f $APPENV ] ; then
   echo "File $APPENV does not exist."
   checkflag=1
fi

if [ ! -f $OTHERENV ] ; then
   echo "File $OTHERENV does not exist."
   checkflag=1
fi

if [ ! -f $SSLCERT ] ; then
   echo "File $SSLCERT does not exist."
   checkflag=1
fi

if [ ! -f ${SSLKEY}.enc ] ; then
   echo "File ${SSLKEY}.enc does not exist."
   checkflag=1
fi

if [ ! -f $SSLCA ] ; then
   echo "File $SSLCA does not exist."
   checkflag=1
fi

if [ ! -f ${APPTARFILE}.enc ] ; then
   echo "File ${APPTARFILE}.enc does not exist."
   checkflag=1
fi

if [ $checkflag -eq 1 ] ; then
   exit 1
fi

aws s3 cp ${APPTARFILE}.enc   $S3BUCKET
aws s3 cp $APPSSH             $S3BUCKET
aws s3 cp ${APPENVFILE}.enc   $S3BUCKET
aws s3 cp $OTHERENV           $S3BUCKET
aws s3 cp $SSLCERT            $S3BUCKET
aws s3 cp ${SSLKEY}.enc       $S3BUCKET
aws s3 cp $SSLCA              $S3BUCKET

aws s3 ls $S3BUCKET

