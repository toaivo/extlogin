resource "aws_iam_server_certificate" "extlogin_cert" {
  name_prefix      = "extlogin"
  certificate_body = file("extlogin.crt")
  private_key      = file("extlogin.key")
  certificate_chain      = file("extlogin-ca.crt")

  lifecycle {
    create_before_destroy = true
  }
}
