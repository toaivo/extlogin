#!/bin/bash
if [ $# -ne 2 ]; then
   echo "Usage $0 host key-pair"
   exit 1
fi

HN=$1
KP=$2

ssh -i $KP admin@{$HN}
